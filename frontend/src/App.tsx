import React, { useContext } from "react";
import "./styles/main.sass";
import NavBar from "./components/NavBar";
import Timetable from "./components/Timetable";
import { AppStoreContext } from "./stores/AppStore";
import { observer } from "mobx-react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { RegistrationPage } from "./components/RegistrationPage";
import { LoginPage } from "./components/LoginPage";
import { RedirectIfLoggedOut } from "./components/ConditionalRedirects";

const App = observer(() => {
    const AppStore = useContext(AppStoreContext);

    return (
        <BrowserRouter>
            <div className="App">
                <NavBar />
                {/*<header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.tsx</code> and save to reload.
                    </p>
                    <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
                        Learn React
                    </a>
                    <div>
                        STORE: {AppStore.getTest().toString()}
                        <button onClick={() => AppStore.setTest(!AppStore.getTest())}>toggle</button>
                    </div>
                </header>*/}
                <div className="content">
                    <Switch>
                        <Route exact path="/program">
                            <Timetable title="Program" talks={AppStore.talks} />
                        </Route>
                        <Route exact path="/myProgram">
                            <RedirectIfLoggedOut target="/login"/>
                            <Timetable title="Tvůj osobní program" talks={AppStore.userSelectedTalks} />
                        </Route>
                        <Route exact path="/login">
                            <LoginPage />
                        </Route>
                        <Route exact path="/register">
                            <RegistrationPage />
                        </Route>
                        <Redirect to="/program" />
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    );
});

export default App;
