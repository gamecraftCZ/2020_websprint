export function sleep(milliseconds: number): void {
    const start = new Date().getTime();
    for (let i = 0; i < 1e7; i++) {
        if (new Date().getTime() - start > milliseconds) {
            break;
        }
    }
}

/**
 * Checks if string is a valid number.
 * @param text
 * @returns {boolean}
 */
export const isNumeric = (text: string): boolean => {
    return !isNaN(Number.parseFloat(text));
};

/**
 * Checks if string is a valid number in Base 10.
 * @param text
 * @returns {boolean}
 */
export const isNumericBase10 = (text: string): boolean => {
    for (const char of text) {
        if (!"0123456789".includes(char)) return false;
    }
    return true;
};

/**
 * Selects random item from array.
 * @param array {Array} Array of items to select from
 * @returns Random item from array
 */
export const randomFromArray = <T>(array: Array<T>): T => array[Math.floor(Math.random() * array.length)];

export const randomInRange = (min: number, max: number): number => Math.floor(Math.random() * max - min) + min;
