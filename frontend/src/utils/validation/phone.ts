const phoneNumberRegExp = /[0-9]{9}/;

export const isValidPhoneNumber = (str: string) => phoneNumberRegExp.test(str.replace(/[ -]/g, ""));
