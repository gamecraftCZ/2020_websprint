/*
 * Scope functions - functions modifying scope of other functionskw
 *
 * The names were taken from Kotlin, and therefore nice reference table can be found here: https://kotlinlang.org/docs/reference/scope-functions.html#function-selection
 */

/**
 * Execute the lambda with the object as parameter and return the lambda's result.
 * @param obj the object passed as parameter
 * @param lambda the lambda to run
 */
export const letS = <ObjectType, LambdaReturnType>(
    obj: ObjectType,
    lambda: (obj: ObjectType) => LambdaReturnType,
) => lambda(obj);

/**
 * Run the lambda and return it's result
 * @param lambda the lambda to run
 */
export const runS = <LambdaReturnType>(lambda: () => LambdaReturnType) => lambda();

/**
 * If the object is not `null` (or `undefined`), execute the lambda with the object as parameter and return the lambda's result, `undefined` otherwise.
 * @param obj the object passed as parameter
 * @param lambda the lambda to run
 */
export const letSIfNotNull = <ObjectType, LambdaReturnType>(
    obj: ObjectType,
    lambda: (obj: NonNullable<ObjectType>) => LambdaReturnType,
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-unnecessary-type-assertion
) => (obj == undefined ? undefined : letS(obj!, lambda));

/**
 * Execute the lambda with the object as parameter and return the object.
 * @param obj the object passed as parameter
 * @param lambda the lambda to run
 */
export const alsoS = <ObjectType>(obj: ObjectType, lambda: (obj: ObjectType) => void) => (
    lambda(obj), obj
);
