import React, { useContext } from "react";
import { AppStoreContext } from "../stores/AppStore";
// @ts-ignore
import { Talk } from "../structures/Talk";
import { ErrorMessage } from "./ErrorMessage";
import { observer } from "mobx-react";
import TalkElement from "./TalkElement";

const Timetable = observer((props: { title: string; talks: Talk[] }) => {
    const AppStore = useContext(AppStoreContext);

    let firstTime = 9999999999999999999999999999;
    let lastTime = 0;
    for (let talk of props.talks) {
        if (talk.start < firstTime) firstTime = talk.start;
        if (talk.end > lastTime) lastTime = talk.end;
    }

    const timeElements = [];
    const firstHour = new Date(firstTime).getHours() - 1;
    const lastHour = new Date(lastTime).getHours();

    console.log(firstTime);
    console.log(lastTime);
    console.log(firstHour);
    console.log(lastHour);

    for (let hour = firstHour; hour <= lastHour; hour++) {
        const fromTop = (hour - firstHour) * 2 + 1;
        console.log(fromTop);
        timeElements.push(<span className="events__time" style={{ gridArea: `${fromTop} / 1 / ${fromTop + 1} / 2` }}>{hour}:00</span>);
        timeElements.push(<span className="events__time" style={{ gridArea: `${fromTop + 1} / 1 / ${fromTop + 2} / 2` }}>{hour}:30</span>);
    }

    const talksElements = [];
    for (const talk of props.talks) {
        let fromTop = (talk.startTimeHours - firstHour) * 2 + 1;
        if (talk.startTimeMinutes === 30) fromTop += 1;

        let fromTopEnd = (talk.endTimeHours - firstHour) * 2 + 1;
        if (talk.endTimeMinutes === 30) fromTopEnd += 1;

        talksElements.push(<TalkElement rowStart={fromTop} columnStart={3} rowEnd={fromTopEnd} columnEnd={4} talk={talk} />);
    }

    if (AppStore.isFetchingTalks) return <h2>Načítám talky</h2>;
    if (props.talks.length === 0) return <ErrorMessage text="Nenalezli jsme žádné talky" />;

    return (
        <>
            <div className="timetable">
                <div className="timetable__day">
                    <header className="timetable__row timetable__day__header">
                        <span className="day__header__date">st 26.02.2020</span>
                    </header>
                    <div className="timetable__day__events">
                        {timeElements}
                        {talksElements}
                    </div>
                </div>
            </div>
        </>
    );
});

export default Timetable;
