import React, { useContext } from "react";
import { AuthStoreContext } from "../stores/AuthStore";
import { Redirect } from "react-router-dom";
import { observer } from "mobx-react";

export const RedirectIfLoggedIn = observer((props: {target: string}) => {
    const AuthStore = useContext(AuthStoreContext);
    return AuthStore.isLoggedIn ? <Redirect to={props.target}/> : null;
});

export const  RedirectIfLoggedOut = observer((props: {target: string}) => {
    const AuthStore = useContext(AuthStoreContext);
    return AuthStore.isLoggedIn ? null : <Redirect to={props.target}/>;
});
