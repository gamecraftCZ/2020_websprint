import React, { FormEvent, useContext } from "react";
import { useTextInputState } from "./forms/TextInput/hooks";
import { PasswordInput, TextInput } from "./forms/TextInput";
import { AuthStoreContext } from "../stores/AuthStore";
import { ErrorMessage } from "./ErrorMessage";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import { RedirectIfLoggedIn } from "./ConditionalRedirects";

export const RegistrationPage = observer(() => {
    const emailState = useTextInputState();
    const passwordState = useTextInputState();
    const AuthStore = useContext(AuthStoreContext);

    const register = (e: FormEvent) => {
        e.preventDefault();

        if (emailState.isValid && passwordState.isValid) {
            AuthStore.register(emailState.value, passwordState.value);
        }
    };

    return (
        <div className="login">
            <div className="login__form register__form">
                <h1>REGISTRACE</h1>
                <form onSubmit={register}>
                    <div className="login__form__element">
                        <span className="login__form__label">email:</span> <TextInput state={emailState} />
                    </div>
                    <div className="login__form__element">
                        <span className="login__form__label">heslo:</span> <PasswordInput state={passwordState} />
                    </div>
                    {AuthStore.registerErrorMessage ? <ErrorMessage text={AuthStore.registerErrorMessage} /> : null}
                    <button className="login__form__button">Registrovat se</button>
                    <div className="register">
                        Už máš účet? <Link to="/login">Přihlas se</Link>
                    </div>
                </form>
                <RedirectIfLoggedIn target="/program" />
            </div>
        </div>
    );
});
