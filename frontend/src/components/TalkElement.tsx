import React, { useState } from "react";
// @ts-ignore
import { Talk } from "../structures/Talk";
import { Checkbox } from "antd";
import { CheckboxChangeEvent } from "antd/es/checkbox";

const TalkElement = (props: { rowStart: number; columnStart: number; rowEnd: number; columnEnd: number; talk: Talk }) => {
    const [force, forceRerender] = useState(1);

    let background = "";
    if (props.columnStart == 2) {
        background = "#C0EBFF";
    } else {
        background = "#9DFCE1";
    }

    return (
        <>
            <div
                className="talk"
                style={{
                    gridArea: `${props.rowStart}/${props.columnStart}/${props.rowEnd}/${props.columnEnd}`,
                    background: background,
                }}
            >
                <span className="talk__element talk__name">{props.talk.name}</span>
                <span className="talk__element talk__description">{props.talk.description}</span>
                <span className="talk__element talk__interested">
                    <span>zajímá mě</span>
                    <Checkbox onChange={(e: CheckboxChangeEvent) => {
                        props.talk.setTalkSelected(!props.talk.selectedByUser);
                        forceRerender(-force);
                    }} checked={props.talk.selectedByUser} className="checkbox" />
                </span>
            </div>
        </>
    );
};

export default TalkElement;
