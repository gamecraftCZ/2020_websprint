import React from "react";

export const InputValidationResults = ({
    validatorResults,
}: {
    validatorResults: readonly { isValid: boolean; message?: string }[];
}) => {
    return (
        <ul className="validator-message-list">
            {validatorResults
                .filter(r => r.message)
                .map(validatorResult => (
                    <li className={validatorResult.isValid ? "validator-message-valid" : "validator-message-invalid"}>
                        {validatorResult.message}
                    </li>
                ))}
        </ul>
    );
};
