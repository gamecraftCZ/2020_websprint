import React from "react";
import { useTextInputState } from "./hooks";
import { InputValidationResults } from "../InputValidationResults";

export const TextInput: React.FC<JSX.IntrinsicElements["input"] & {
    state: ReturnType<typeof useTextInputState>;
}> = ({ state: { value, setValue, isValid, isTouched, validatorResults }, ...inputProps }) => (
    <>
        <input
            value={value}
            onChange={({ target: { value } }) => {
                setValue(value);
            }}
            type="text"
            {...inputProps}
            className={`input${isTouched && !isValid ? " input-invalid" : ""}`}
        />
        <InputValidationResults validatorResults={validatorResults} />
    </>
);

export const PasswordInput: typeof TextInput = inputProps => <TextInput type="password" {...inputProps} />;
