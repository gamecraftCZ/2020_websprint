import { Validator } from "..";
import { letS } from "../../../../../utils/scopeFunctions";

export type ValidatorMessages = { valid?: string; invalid?: string };

const getMessage = (isValid: boolean, { valid, invalid }: ValidatorMessages) => (isValid ? valid : invalid);

/**
 * Compose "validator generator" for given condition
 *
 * "validator generator" is function like `minLength` which will take some arguments (like the required length) and returns validator which will validate string for that length.
 *
 * The main purpose for this function is to abstract away the handling of valid / invalid messages.
 *
 * @param condition the condition to decide whether some string is valid for given options.
 */
export const composeValidatorGenerator = <TValue>(
    condition: (validatedString: string, value: TValue, preventPropagation?: () => void) => boolean,
) => (value: TValue, messages: ValidatorMessages): Validator => (validatedString, preventPropagation) =>
    letS(condition(validatedString, value, preventPropagation), isValid => ({
        isValid,
        message: getMessage(isValid, messages),
    }));

/**
 * Compose "validator generator" for given condition which doesn't take any parameters
 *
 * "validator generator" is function like `isIsbn` which will take some arguments (like the required length) and returns validator which will validate string for that length.
 *
 * The main purpose for this function is to abstract away the handling of valid / invalid messages.
 *
 * @param condition the condition to decide whether some string is valid for given options.
 */
export const composeParameterlessValidatorGenerator = (
    condition: (validatedString: string, preventPropagation?: () => void) => boolean,
) => (messages: ValidatorMessages): Validator => (validatedString, preventPropagation) =>
    letS(condition(validatedString, preventPropagation), isValid => ({
        isValid,
        message: getMessage(isValid, messages),
    }));
