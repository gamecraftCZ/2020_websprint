import { isValidPhoneNumber } from "../../../../../utils/validation/phone";
import { composeParameterlessValidatorGenerator, composeValidatorGenerator } from "./utilities";

export const minLength = composeValidatorGenerator((str, len: number) => str.length >= len);
export const maxLength = composeValidatorGenerator((str, len: number) => str.length <= len);
export const regexp = composeValidatorGenerator((str, regexp: RegExp) => regexp.test(str));
export const isRequired = composeValidatorGenerator((str, isRequired: boolean, preventPropagation) => {
    if (str.length === 0)
        if (isRequired) return false;
        else preventPropagation?.();
    return true;
});
export const isPhoneNumber = composeParameterlessValidatorGenerator(isValidPhoneNumber);
export const minNumber = composeValidatorGenerator((str, number: number) => parseInt(str) >= number);
export const maxNumber = composeValidatorGenerator((str, number: number) => parseInt(str) <= number);
export const equals = composeValidatorGenerator((str, equals: string) => str === equals);

export const commonValidators = {
    minLength,
    maxLength,
    regexp,
    isRequired,
    isPhoneNumber,
    minNumber,
    maxNumber,
    equals,
} as const;
