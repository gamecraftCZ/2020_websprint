import { useMemo, useState } from "react";

/**
 * Result of performing validation
 *
 * The message, if provided, is shown to user as error message if the validation failed or success message if it succeeded.
 */
type ValidatorResult = { isValid: boolean; message?: string };

/**
 * Text input validator
 *
 * @param preventPropagation Function, which when called will stop running other validators for this validation
 */
export type Validator = (validatedString: string, preventPropagation?: () => void) => ValidatorResult;

/**
 * Result of validator which combines miltiple validators into one. isValid blah
 */
type CombinedValidatorResult = {
    /**
     * False if at least one of the validators has failed, true otherwise.
     */
    wereAllSuccessful: boolean;
    validatorResults: ValidatorResult[];
};

/**
 * Validator which combines multiple validators
 */
export type CombinedValidator = (validatedString: string) => CombinedValidatorResult;

/**
 * Combine multiple validators into one
 *
 * The order in which the validators are run is in the order they were passed.
 */
const combineValidators = (validators: readonly Validator[]) => (validatedString: string) => {
    const validatorResults: ValidatorResult[] = [];
    let wereAllSuccessful = true;

    for (const validator of validators) {
        let isPropagationStopped = false;
        const validatorResult = validator(validatedString, () => {
            isPropagationStopped = true;
        });
        if (wereAllSuccessful && !validatorResult.isValid) wereAllSuccessful = false;
        validatorResults.push(validatorResult);
        if (isPropagationStopped) break;
    }
    return { wereAllSuccessful, validatorResults };
};

/**
 * Create state object usable by the `TextInput` component
 *
 * @param validator the validator function for the input
 * @param initialValue the initial value for the input
 */
export const useTextInputState = (validators: readonly Validator[] = [], initialValue = "") => {
    const [value, setValue] = useState(initialValue);
    const [isTouched, setIsTouched] = useState(false);
    const combinedValidator = useMemo(() => combineValidators(validators), validators);
    const [isValid, setIsValid] = useState(useMemo(() => combinedValidator(initialValue).wereAllSuccessful, []));
    const [validatorResults, setValidatorResults] = useState<readonly ValidatorResult[]>([]);

    const setValueAndValidate = (newValue: string) => {
        const trimmedNewValue = newValue.trim();
        setValue(trimmedNewValue);
        setIsTouched(true);
        const combinedValidatorResult = combinedValidator(trimmedNewValue);
        setIsValid(combinedValidatorResult.wereAllSuccessful);
        setValidatorResults(combinedValidatorResult.validatorResults);
    };

    return {
        value,
        setValue: setValueAndValidate,
        isTouched,
        setIsTouched,
        isValid,
        validatorResults,
    };
};
