import React, { FormEvent, useContext } from "react";
import { useTextInputState } from "./forms/TextInput/hooks";
import { PasswordInput, TextInput } from "./forms/TextInput";
import { AuthStoreContext } from "../stores/AuthStore";
import { ErrorMessage } from "./ErrorMessage";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import { RedirectIfLoggedIn } from "./ConditionalRedirects";
import { AppStoreContext } from "../stores/AppStore";

export const LoginPage = observer(() => {
    const emailState = useTextInputState();
    const passwordState = useTextInputState();
    const AuthStore = useContext(AuthStoreContext);
    const AppStore = useContext(AppStoreContext);

    const login = async (e: FormEvent) => {
        e.preventDefault();

        if (emailState.isValid && passwordState.isValid) {
            await AuthStore.login(emailState.value, passwordState.value);
            await AppStore.fetchUserTalks();
        }
    };
    return (
        <div className="login">
            <div className="login__form">
                <h1>TEP</h1>
                <p>techheaven event planner</p>
                <form onSubmit={login}>
                    <div className="login__form__element">
                        <span className="login__form__label">email:</span> <TextInput state={emailState} />
                    </div>
                    <div className="login__form__element">
                        <span className="login__form__label">heslo:</span> <PasswordInput state={passwordState} />
                    </div>
                    {AuthStore.loginErrorMessage ? <ErrorMessage text={AuthStore.loginErrorMessage} /> : null}
                    <button className="login__form__button">přihlásit se</button>
                    <div className="register">
                        <Link to="/register">zaregistruj se</Link>
                    </div>
                </form>
            </div>
            <RedirectIfLoggedIn target="/myProgram" />
        </div>
    );
});
