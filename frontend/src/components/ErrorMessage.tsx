import React from "react";

export const ErrorMessage = ({ text }: { text: string }) => {
    return <div className="ErrorMessage">{text}</div>;
};
