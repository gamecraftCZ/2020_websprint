import { Room } from "./Room";
import axios from "axios";
import { serverUrl } from "../config";
import { AuthStore } from "../stores/AuthStore";
import { AppStore } from "../stores/AppStore";

export class Talk {
    id: number;
    start: number;
    end: number;

    name: string;
    lecturer: string;
    description: string;
    room: Room;

    selectedByUser: boolean;

    setTalkSelected = async (selected: boolean) => {
        this.selectedByUser = selected;
        AppStore.forceRerenderTalks();

        console.log({headers: {Authorization: AuthStore.token}});

        try {
            const response = await axios.post(`${serverUrl}/setTalk.php`, {id: this.id, going: selected}, {headers: {Authorization: AuthStore.token}});
            if (response.status === 200) {
                console.log(response);
            } else {
                console.error("setTalkSelected error: ", response);
                this.selectedByUser = !selected;
                AppStore.forceRerenderTalks();
            }
        } catch (e) {
            console.error("setTalkSelected error: ", e);
            this.selectedByUser = !selected;
            AppStore.forceRerenderTalks();
        }
    };

    get startTimeHours() {
        return (new Date(this.start)).getHours();
    }
    get startTimeMinutes() {
        return (new Date(this.start)).getMinutes();
    }

    get endTimeHours() {
        return (new Date(this.end)).getHours();
    }
    get endTimeMinutes() {
        return (new Date(this.end)).getMinutes();
    }


    constructor(id: number, start: number, end: number, name: string, lecturer: string, description: string, room: Room, selectedByUser: boolean = false) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.name = name;
        this.lecturer = lecturer;
        this.description = description;
        this.room = room;
        this.selectedByUser = selectedByUser;
    }

    static fromServerObject(obj: any): Talk {
        return new Talk(obj.id, obj.start, obj.end, obj.name, obj.lecturer, obj.description, Room.fromServerObject(obj.room));
    }
}
