
export class Room {
    id: number;
    name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }

    static fromServerObject(obj: any): Room {
        return new Room(obj.id, obj.name);
    }
}