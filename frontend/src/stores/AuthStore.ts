import { action, computed, observable } from "mobx";
import { createContext } from "react";
import { serverUrl } from "../config";
import Cookie from "mobx-cookie";
import axios from "axios";

export class AuthStoreClass {
    private tokenCookie: Cookie = new Cookie("token");
    @observable username: string | null = null;
    @observable userId: number | null = null;

    @observable loggingIn: boolean = false;
    @observable loginErrorMessage: string | null = null;
    @observable registerErrorMessage: string | null = null;

    @computed get token(): string {
        return this.tokenCookie.value || "";
    }
    set token(token: string) {
        this.tokenCookie.set(token);
    }

    @computed get isLoggedIn(): boolean {
        return !!this.token;
    }
    @computed get isLoginError(): boolean {
        return !!this.loginErrorMessage;
    }
    @computed get isRegisterError(): boolean {
        return !!this.registerErrorMessage;
    }

    @action logout = () => {
        console.log("logout", this.token);
        this.token = "";
    };

    @action async login(email: string, password: string) {
        try {
            const response = await axios.post(`${serverUrl}/login.php`, {email, password});
            if (response.status === 200) {
                console.log(response);
                if (response.data.error === "Invalid username") {
                    this.loginErrorMessage = "Tento email není ještě zaregistrován."
                } else {
                    this.token = response.data.token;
                }
            } else {
                this.loginErrorMessage = "Stala se chyba, zkuste to znovu.";
                console.error("login error: ", response);
            }
        } catch (e) {
            this.loginErrorMessage = "Stala se chyba sítě, zkuste to znovu.";
            console.error("login error: ", e);
        }
    }

    @action async register(email: string, password: string) {
        try {
            const response = await axios.post(`${serverUrl}/register.php`, {email, password});
            if (response.status === 200) {
                this.token = response.data.token;
            } else {
                this.registerErrorMessage = "Stala se chyba, zkuste to znovu.";
                console.error("register error: ", response);
            }
        } catch (e) {
            this.registerErrorMessage = "Stala se chyba sítě, zkuste to znovu.";
            console.error("register error: ", e);
        }
    }
}

export const AuthStore = new AuthStoreClass();
export const AuthStoreContext = createContext(AuthStore);
