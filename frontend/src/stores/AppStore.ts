import { action, computed, observable } from "mobx";
import { createContext } from "react";
// @ts-ignore
import { Talk } from "../structures/Talk";
import axios from "axios";
import { serverUrl } from "../config";
import { AuthStore } from "./AuthStore";

export class AppStoreClass {
    @observable private talksList: Talk[] = [];
    @observable isFetchingTalks: boolean = false;

    @observable fetchingTalksError: string = "";

    @computed get talks(): Talk[] {
        return this.talksList;
    }

    @computed get userSelectedTalks(): Talk[] {
        return (this.talks?.filter(talk => talk.selectedByUser)) || [];
    }

    @action async fetchTalks(): Promise<Talk[]> {
        this.isFetchingTalks = true;

        try {
            const response = await axios.post(`${serverUrl}/getAllTalks.php`);
            if (response.status === 200) {
                console.log(response);
                this.talksList = response.data.talks.map((talk: any) => Talk.fromServerObject(talk));
            } else {
                this.fetchingTalksError = "Stala se chyba, zkuste to znovu.";
                console.error("fetchTalks error: ", response);
            }
        } catch (e) {
            this.fetchingTalksError = "Stala se chyba sítě, zkuste to znovu.";
            console.error("fetchTalks error: ", e);
        }

        this.isFetchingTalks = false;
        return this.talksList;
    }

    @action forceRerenderTalks() {
        this.talksList = [...this.talksList];
    }

    @action async fetchUserTalks(): Promise<Talk[]> {
        try {
            const response = await axios.post(`${serverUrl}/getUserTalks.php`, {}, {headers: {Authorization: AuthStore.token}});
            if (response.status === 200) {
                console.log(response);
                response.data.talks.forEach( (talkIdString: string) => {
                    console.log("Meaw");
                    console.log(talkIdString);
                    const talkId = Number.parseInt(talkIdString);
                    this.talks.forEach(talk => {
                        console.log(talkId, talk.id);
                        if (talk.id === talkId) {
                            console.log("AHOJ");
                            talk.selectedByUser = true;
                        }
                    })
                });
            } else {
                console.error("fetchUserTalks error: ", response);
            }
        } catch (e) {
            console.error("fetchUserTalks error: ", e);
        }

        this.forceRerenderTalks();

        return this.userSelectedTalks;
    }

    constructor() {
        this.fetchTalks();
        if (AuthStore.isLoggedIn) this.fetchUserTalks();
    }
}

export const AppStore = new AppStoreClass();
export const AppStoreContext = createContext(AppStore);
