<?php
require "tokens.php";
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Origin', '*');
header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers',' Origin, Content-Type, Accept, Authorization, X-Request-With');
header('Access-Control-Allow-Credentials',' true');
//ini_set('display_errors', 0);
$data = json_decode(file_get_contents("php://input"));
$email = $data -> email;
$password = $data -> password;
$db = db_connect();


/* Prepared statement, stage 1: prepare */
$sql = "INSERT INTO websprint.user(email, password) VALUES (?, ?)";

// TODO handle duplicate email

if ($stmt = $db->prepare($sql)) {
    // TODO remove this duplicate code
    $pass_hash = password_hash($password, PASSWORD_DEFAULT, ['salt' => "dijosfheohreguriehgierhguireguire"]);
    $stmt->bind_param("ss", $email, $pass_hash);
    $stmt->execute();

    $new_id = $last_id = $db->insert_id;
    if ($new_id) {
        $jwt = encode([
            "id" => $new_id,
            "email" => $email
        ]);
        $result = new \stdClass();
        $result -> token = $jwt;
        echo json_encode($result);
    } else {
        http_response_code( 400 );
    }

}

$db -> close();


?>
