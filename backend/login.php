<?php
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Origin', '*');
header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers',' Origin, Content-Type, Accept, Authorization, X-Request-With');
header('Access-Control-Allow-Credentials',' true');
$result = new \stdClass();

require "tokens.php";
//ini_set('display_errors', 0);
$data = json_decode(file_get_contents("php://input"));
$input_email = $data -> email;
$input_password = $data -> password;
$db = db_connect();

$sql = "select id, email, password from websprint.user where email=? limit 1";
$stmt = $db->prepare($sql);
$stmt->bind_param("s", $input_email);
$stmt -> execute() &&
$stmt -> store_result() &&
$stmt -> bind_result($id, $email, $password);

if($stmt->num_rows === 0) {
    $result -> error = "Invalid username";
    echo json_encode($result);
}

while($stmt->fetch()){
    $pass_hash = password_hash($input_password, PASSWORD_DEFAULT, ['salt' => "dijosfheohreguriehgierhguireguire"]);

    if ($pass_hash == $password) {
        $jwt = encode([
            "id" => $id,
            "email" => $email
        ]);
        $result = new \stdClass();
        $result -> token = $jwt;
        echo json_encode($result);
        return;
    } else {
        $result -> error = "Invalid password";
        echo json_encode($result);
    }
}
/*
while($row = $result->fetch_assoc()) {
    $pass_hash = password_hash($password, PASSWORD_DEFAULT, ['salt' => "dijosfheohreguriehgierhguireguire"]);

    if ($pass_hash == $row['password']) {
        $jwt = encode([
            "id" => $row['id'],
            "email" => $row['password']
        ]);
        $result = new \stdClass();
        $result -> token = $jwt;
        echo json_encode($result);
    } else {
        echo "wrong";
    }
}
*/