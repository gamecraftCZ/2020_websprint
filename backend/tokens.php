<?php

require 'vendor/autoload.php';

use \Firebase\JWT\JWT;


// This is your id token

function encode($token_payload)
{
    // This is your client secret
    $key = '__test_secret__';
    return JWT::encode($token_payload, base64_decode(strtr($key, '-_', '+/')), 'HS256');
}

function decode($token) {
    // This is your client secret
    $key = '__test_secret__';
    return JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);
}

function db_connect() {
    $servername = "socialreaders-dev.cplz0ji3sclm.eu-central-1.rds.amazonaws.com";
    $username = "websprint";
    $password = "websprint2020";

    $db = mysqli_init();
    $db->ssl_set(NULL,NULL,'rds-ca-2019-root.pem',NULL,NULL);
    $db->real_connect($servername,$username,$password,"websprint");
    mysqli_set_charset( $db, "utf8mb4");

    // Check connection
    if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error);
    }
    //echo "Connected successfully";

    return $db;
}