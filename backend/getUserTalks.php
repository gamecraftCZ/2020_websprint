<?php
require "tokens.php";
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Origin', '*');
header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers',' Origin, Content-Type, Accept, Authorization, X-Request-With');
header('Access-Control-Allow-Credentials',' true');
$headers = apache_request_headers();
$token = $headers['Authorization'];

$db = db_connect();

try {
    $token_payload = decode($token);
    $user_id = $token_payload -> id;
} catch (Throwable $e) {
    echo $e -> getMessage();
    //http_response_code( 403 );
    return;

}

// body
try {
    $sql = "select talk_id from websprint.user_talk where user_id=?";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("i", $user_id);
    $stmt -> execute() &&
    $stmt -> store_result() &&
    $stmt -> bind_result($talk_id);

    $talks = [];
    while($stmt->fetch()){
        $talks[] = $talk_id;
    }

    $result = new \stdClass();
    $result -> reponse = "Success";
    $result -> talks = $talks;
    echo json_encode($result);

} catch (Throwable $e) {
    echo $e->getMessage();
    http_response_code( 500 );
}

