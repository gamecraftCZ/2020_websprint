<?php
require "tokens.php";
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Origin', '*');
header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers',' Origin, Content-Type, Accept, Authorization, X-Request-With');
header('Access-Control-Allow-Credentials',' true');
$db = db_connect();

// body
try {
    $sql = "select talk.id, talk.name, unix_timestamp(start) * 1000, unix_timestamp(end) * 1000, lecturer, description, room_id, 
       room.name as room_name from websprint.talk left join websprint.room on talk.room_id = room.id";
    $stmt = $db->prepare($sql);
    $stmt->execute() &&
    $stmt->store_result() &&
    $stmt->bind_result($id, $name, $start, $end, $lecturer, $description, $room_id, $room_name);

    $talks = [];
    while ($stmt->fetch()) {
        $result = new \stdClass();
        $result->id = $id;
        $result->name = $name;
        $result->start = $start;
        $result->end = $end;
        $result->lecturer = $lecturer;
        $result->description = $description;
        $result->room = new \stdClass();
        $result->room->id = $room_id;
        $result->room->name = $room_name;

//        print_r($result);
        $talks[] = $result;
    }

    $result = new \stdClass();
    $result -> response = "Success";
    $result -> talks = $talks;
    echo json_encode($result);

} catch (Throwable $e) {
    echo $e->getMessage();
    http_response_code(500);
}

