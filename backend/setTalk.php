<?php
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code( 200 );
    return;
}

require "tokens.php";
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

$headers = apache_request_headers();
$token = $headers['Authorization'];
$data = json_decode(file_get_contents("php://input"));
$id_talk = $data -> id;
$going = $data -> going;
$db = db_connect();
try {
    $token_payload = decode($token);
    $user_id = $token_payload -> id;
} catch (Throwable $e) {
    echo $e -> getMessage();

    http_response_code( 403 );
    return;

}

// body
try {
    if ($going) {
        $sql = "INSERT ignore INTO websprint.user_talk(user_id, talk_id) VALUES (?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $user_id, $id_talk);
        $stmt->execute();
    } else {
        $sql = "delete from websprint.user_talk where user_id=? and talk_id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $user_id, $id_talk);
        $stmt->execute();
    }

    if ($db->error) {
        echo $db->error;
        throw new InvalidArgumentException("invalid arg");
    }


    $result = new \stdClass();
    $result -> response = "Success";
    echo json_encode($result);

} catch (Throwable $e) {
    echo $e->getMessage();
    http_response_code( 500 );
}

